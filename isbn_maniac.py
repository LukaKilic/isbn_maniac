####################################
# ISBN maniac is a program that    #
# can convert 10 digit to 13 digit #
# ISBN and vice versa. It can also #
# find the appropriate check digit #
# value whether it's a 10 digit or #
# a 13 digit ISBN                  #
####################################

import sys


# Accepts 10 digit ISBN without check digit and calculates it
def find_check_ten(isbn_ten):
    isbn_list = [i for i in isbn_ten if i.isdigit()]
    total = 0
    for i in range(1, 10):
        total += int(isbn_list[i-1]) * i
    for i in range(11):
        if (total + i * 10) % 11 == 0:
            return str(i)


# Checks if check digit is correct
def check_digit_ten():
    isbn = raw_input('Enter a 10 digit ISBN: ').strip()
    check_digit = find_check_ten(isbn[:-1])
    if (check_digit == 10 and isbn[-1].lower() == 'x') or \
        check_digit == isbn[-1]:
        print 'Check digit is correct'
    else:
        print 'Check digit is wrong, it should be: ' + str(check_digit)


# Accepts 13 digit ISBN without check digit and calculates it
def find_check_thirteen(isbn_thirteen):
    isbn_list = [i for i in isbn_thirteen if i.isdigit()]
    total = 0
    for i in range(1, 13):
        if i % 2 != 0:
            total += int(isbn_list[i-1]) * 1
        else:
            total += int(isbn_list[i-1]) * 3
    for i in range(10):
        if (total + i) % 10 == 0:
            return str(i)


# Checks if check digit is correct
def check_digit_thirteen():
    isbn = raw_input('Enter a 13 digit ISBN: ').strip()
    check_digit = find_check_thirteen(isbn[:-1])
    if check_digit == isbn[-1]:
        print 'Check digit is correct'
    else:
        print 'Check digit is wrong, it should be: ' + str(check_digit)


# Converts 10 digit ISBN to 13 digit ISBN
def ten_to_thirteen():
    isbn = raw_input('Enter a 10 digit ISBN you want to convert: ').strip()
    isbn_temp = '978-' + isbn[:-1]
    isbn_thirteen = isbn_temp + find_check_thirteen(isbn_temp)
    print '{0} ISBN 10 is {1} ISBN 13'.format(isbn, isbn_thirteen)


# Converts 13 digit ISBN to 10 digit ISBN
def thirteen_to_ten():
    isbn = raw_input('Enter a 13 digit ISBN you want to convert: ').strip()
    isbn_temp = isbn.lstrip('978-')[:-1]
    isbn_ten = isbn_temp + find_check_ten(isbn_temp)
    print '{0} ISBN 13 is {1} ISBN 10'.format(isbn, isbn_ten)


def exit_program():
    sys.exit()


def print_menu():
    print '   \nPython ISBN Conversion Menu'
    print '1. Verify the check digit of an ISBN-10'
    print '2. Verify the check digit of an ISBN-13'
    print '3. Convert an ISBN-10 to an ISBN-13'
    print '4. Convert an ISBN-13 to an ISBN-10'
    print '5. Exit\n'


def main():
    choices = {'1': check_digit_ten, '2': check_digit_thirteen,
               '3': ten_to_thirteen, '4': thirteen_to_ten, '5': exit_program}
    while True:
        print_menu()
        choice = raw_input('Select a number for a wanted action: ')
        try:
            choices[choice]()
        except KeyError:
            print 'Unsupported selection number'


if __name__ == '__main__':
    main()
